# `codingpaws/spectron-ci`

Use Electron’s Spectron in your CI pipelines.

## Usage

You can pull the image using `docker pull codingpaws/spectron-ci:latest`.

See the [`codingpaws/spectron-ci`](https://hub.docker.com/r/codingpaws/spectron-ci) repository on Docker Hub.
